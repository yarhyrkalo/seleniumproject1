﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.IO;

namespace SeleniumProject1._1
{
    public class Tests
    {

        IWebDriver driver = new ChromeDriver();

        [SetUp]
        public void Initialize()
        {
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl("https://www.google.com/");
        }

        [Test]
        public void ExecuteTest()
        {
            IWebElement searchBar = driver.FindElement(By.XPath("//input[@title='Пошук']"));

            searchBar.SendKeys("cats" + Keys.Enter);

            
            IWebElement imagePage = driver.FindElement(By.XPath("//a[contains(text(), 'Зображення')]"));

            imagePage.Click();


            IWebElement imagesOnPage = driver.FindElement(By.XPath("//div[@id='islrg']//img"));

            Screenshot screenshot = ((ITakesScreenshot)driver).GetScreenshot();
            screenshot.SaveAsFile("C://Image.png", ScreenshotImageFormat.Png);


            Assert.True(imagesOnPage.Displayed);
        }

        [TearDown]
        public void EndTest()
        {
            driver.Close();
        }
    }
}